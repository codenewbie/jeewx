JeeWx, 微信管家平台，简称“捷微”.
===============
捷微是一款免费开源的JAVA微信公众账号开发平台.
===============

![github](http://img.blog.csdn.net/20140706133601296?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvemhhbmdkYWlzY290dA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center "jeewx")
<br>平台介绍：

一、简介
-----------------------------------
Jeewx是一个开源、高效、敏捷的微信开发平台，采用JAVA语言基于Jeecg快速开发框架实现，Jeewx开源版实现了微信平台的基础功能，便于用户二次开发。jeewx采用插件的方式实现微信功能，不同的插件实现不同的微信功能。支持微信第三方平台全网发布、支持微信插件开发机制，可轻松集成微信H5插件。

主要特性
-----------------------------------
* 	1、基于快速开发平台jeecg 3.4.4最新版本，采用SpringMVC+Hibernate4+UI库+代码生成器+Jquery+Ehcache等主流架构技术
*   2、支持企业快速开发，完善的用户组织机构，报表，强大的代码生成器快速有效的提高开发效率
*   3、开源免费，jeewx遵循Apache2开源协议,免费提供使用。
*   4、支持多用户多公众号管理
*   5、详细的二次开发文档，并不断更新增加相关开发案例提供学习参考
*   6、微信功能插件化开发，更易于定制和二次开发
*   7、支持微信第三方平台全网发布
*   8、支持author2.0机制

主要功能
-----------------------------------
*   1，微信接口认证
*   2，菜单自定义
*   3，文本管理和回复
*   4，关注欢迎语
*   5，关键字管理
*   6，文本模板管理
*   7，图文模板管理
*   8，账号管理
*   9，用户管理
*   10，角色管理
*   11，菜单管理
*   12, 微信支持多用户多公众号
*   13，微信大转盘
*   14，微信刮刮乐
*   15，微信CMS
*   16，自定义接口回复
*   17，翻译
*   18，天气
*   21, author2.0链接
*   22, 微信插件机制
*   23, 微信接受消息管理
    


开发环境
-----------------------------------
* 	Tomcat6.0 或更高版本。
* 	MySQL 5.0 或更高版本。
* 	MyEclipse8.5或其他版本。 



商业版介绍
-----------------------------------
*   演示地址：[http://www.jeewx.com/jeewx](http://www.jeewx.com/jeewx)
*   体验账号：ceshi/123456
*   商务QQ：418799587

系统安装
-----------------------------------
* 	1、将项目导入myeclipse8.5。
* 	2、首次在浏览器中访问：
*      http://localhost:8080/jeewx/，采用admin默认密码登录，一个账号只能配置一个微信公众账号。
* 	3、微信域名配置（重要）
* 	   修改：src/sysConfig.properties
* 	   参数：domain={http://localhost:8080/jeewx/}
* 	4、服务器配置      
        URL:   http://*地址*/jeewx/wechatController.do?wechat<br>
        Token:  jeecg<br>

联系方式
-----------------------------------
* 	QQ 群:  289709451
* 	官 网:  www.jeewx.com
* 	邮 箱： jeecg@sina.com
*  商务QQ:  418799587


![github](http://img.blog.csdn.net/20140706133652718?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvemhhbmdkYWlzY290dA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center "jeewx")
![github](http://img.blog.csdn.net/20140706133543390?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvemhhbmdkYWlzY290dA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center "jeewx")